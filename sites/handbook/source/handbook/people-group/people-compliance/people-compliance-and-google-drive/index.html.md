---
layout: handbook-page-toc
title: "People Compliance and Google Drive"
description: "The People Compliance team manages and ensure policies to be compliant in our team member-related document storage and retention in Google Drive."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
